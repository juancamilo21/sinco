﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SincoABRColegio.Migrations
{
    public partial class Migracion2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "FechaIngreso",
                table: "Profesor",
                type: "date",
                nullable: false,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 50);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "FechaIngreso",
                table: "Profesor",
                unicode: false,
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "date");
        }
    }
}
