﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SincoABRColegio.DTO
{
    public class ProfesorDTO
    {

        public int Id { get; set; }
        public string NombreCompleto { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }

        
    }
}
