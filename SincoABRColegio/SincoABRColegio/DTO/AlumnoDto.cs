﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SincoABRColegio.DTO
{
    public class AlumnoDTO
    {
        public int Id { get; set; }
        public string NombreCompleto { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
        public string Grado { get; set; }
        public DateTime FechaNacimiento { get; set; }
    }
}
