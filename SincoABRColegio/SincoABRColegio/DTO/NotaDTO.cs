﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SincoABRColegio.DTO
{
    public class NotaDTO
    {
        public int Id { get; set; }
        public int IdAlumno { get; set; }
        public int IdMateria { get; set; }
        public int Periodo { get; set; }
        public int? Nota1 { get; set; }
        public int? Promedio { get; set; }
    }
}
