﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SincoABRColegio.DTO
{
    public class MateriaDTO
    {


        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdProfesor { get; set; }

       
    }
}
