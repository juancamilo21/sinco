﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SincoABRColegio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MateriaController : ControllerBase
    {
        private Models.SincoABRContext _db;

        public MateriaController(Models.SincoABRContext db)
        {
            _db = db;
        }


        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var lst = from d in _db.Materia
                      select d;

            return Ok(lst);
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<string>> Get(int id)
        {


            var lst = (from d in _db.Materia
                       where id == d.Id
                       select d).SingleOrDefault();

            return Ok(lst);


        }

        [HttpPost]
        public ActionResult Post([FromBody] DTO.MateriaDTO model)
        {
            try
            {
                Models.Materia oMateria = new Models.Materia();
                oMateria.Nombre = model.Nombre;
                oMateria.IdProfesor = model.IdProfesor;
                _db.Materia.Add(oMateria);
                _db.SaveChanges();

            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al crear");
            }
            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody]  DTO.MateriaDTO model)
        {
            try
            {

                Models.Materia oMateria = _db.Materia.Find(model.Id);
                oMateria.Nombre = model.Nombre;
                oMateria.IdProfesor = model.IdProfesor;
                _db.Entry(oMateria).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _db.SaveChanges();




            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al editar");
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int Id)
        {
            try
            {

                Models.Materia oMateria = _db.Materia.Find(Id);
                _db.Materia.Remove(oMateria);
                _db.SaveChanges();

            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al Eliminar");
            }
            return Ok();
        }
    }
}