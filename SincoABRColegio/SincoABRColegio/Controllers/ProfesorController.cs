﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SincoABRColegio.DTO
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfesorController : ControllerBase
    {
        private Models.SincoABRContext _db;

        public ProfesorController (Models.SincoABRContext db)
        {
            _db = db;
        }


        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var lst = from d in _db.Profesor
                      select d;

            return Ok(lst);
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<string>> Get(int id)
        {


            var lst = (from d in _db.Profesor
                       where id == d.Id
                       select d).SingleOrDefault();

            return Ok(lst);


        }

        [HttpPost]
        public ActionResult Post([FromBody] DTO.ProfesorDTO model)
        {
            try
            {
                Models.Profesor oProfesor = new Models.Profesor();
                oProfesor.NombreCompleto = model.NombreCompleto;
                oProfesor.FechaIngreso = model.FechaIngreso;
                oProfesor.Telefono = model.Telefono;
                oProfesor.Email = model.Email;
                _db.Profesor.Add(oProfesor);
                _db.SaveChanges();

            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al crear");
            }
            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody]  DTO.ProfesorDTO model)
        {
            try
            {

                Models.Profesor oProfesor = _db.Profesor.Find(model.Id);
                oProfesor.NombreCompleto = model.NombreCompleto;
                oProfesor.FechaIngreso = model.FechaIngreso;
                oProfesor.Telefono = model.Telefono;
                oProfesor.Email = model.Email;
                _db.Entry(oProfesor).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _db.SaveChanges();




            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al editar");
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int Id)
        {
            try
            {

                Models.Profesor oProfesor = _db.Profesor.Find(Id);
                _db.Profesor.Remove(oProfesor);
                _db.SaveChanges();

            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al Eliminar");
            }
            return Ok();
        }
    }
}