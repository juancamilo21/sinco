﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SincoABRColegio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotaController : ControllerBase
    {
         private Models.SincoABRContext _db;

        public NotaController(Models.SincoABRContext db)
        {
            _db = db;
        }


        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var lst = from d in _db.Nota
                      select d;

            return Ok(lst);
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<string>> Get(int id)
        {


            var lst = (from d in _db.Nota
                       where id == d.Id
                       select d).SingleOrDefault();

            return Ok(lst);


        }

        [HttpPost]
        public ActionResult Post([FromBody] DTO.NotaDTO model)
        {
            try
            {
                Models.Nota oNota = new Models.Nota();
                oNota.IdAlumno = model.IdAlumno;
                oNota.IdMateria = model.IdMateria;
                oNota.Periodo = model.Periodo;
                oNota.Nota1 = model.Nota1;
                oNota.Promedio = model.Promedio;

                _db.Nota.Add(oNota);
                _db.SaveChanges();

            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al crear");
            }
            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody]  DTO.NotaDTO model)
        {
            try
            {

                Models.Nota oNota = _db.Nota.Find(model.Id);
                oNota.IdAlumno = model.IdAlumno;
                oNota.IdMateria = model.IdMateria;
                oNota.Periodo = model.Periodo;
                oNota.Nota1 = model.Nota1;
                oNota.Promedio = model.Promedio;
                _db.Entry(oNota).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _db.SaveChanges();




            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al editar");
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int Id)
        {
            try
            {

                Models.Nota oNota = _db.Nota.Find(Id);
                _db.Nota.Remove(oNota);
                _db.SaveChanges();

            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al Eliminar");
            }
            return Ok();
        }
    }
}