﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SincoABRColegio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlumnoController : ControllerBase
    {
        private Models.SincoABRContext _db;

        public AlumnoController(Models.SincoABRContext db)
        {
            _db = db;
        }


        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var lst = from d in _db.Alumno
                      select d;

            return Ok(lst);
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<string>> Get(int id)
        {
           

                var lst = (from d in _db.Alumno
                           where id == d.Id
                           select d).SingleOrDefault();

                return Ok(lst);
            

        }

        [HttpPost]
        public ActionResult Post([FromBody] DTO.AlumnoDTO  model)
        {
            try
            {
                Models.Alumno oAlumno = new Models.Alumno();
                oAlumno.NombreCompleto = model.NombreCompleto;
                oAlumno.Telefono = model.Telefono;
                oAlumno.Direccion = model.Direccion;
                oAlumno.Email = model.Email;
                oAlumno.Grado = model.Grado;
                oAlumno.FechaNacimiento = model.FechaNacimiento;
                _db.Alumno.Add(oAlumno);
                _db.SaveChanges();            

            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al crear");
            }
            return Ok();
        }

        [HttpPut]
        public ActionResult Put([FromBody]  DTO.AlumnoDTO model)
        {
            try
            {
              
                Models.Alumno oAlumno = _db.Alumno.Find(model.Id);
                oAlumno.NombreCompleto = model.NombreCompleto;
                oAlumno.Telefono = model.Telefono;
                oAlumno.Direccion = model.Direccion;
                oAlumno.Email = model.Email;
                oAlumno.Grado = model.Grado;
                oAlumno.FechaNacimiento = model.FechaNacimiento;
                _db.Entry(oAlumno).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _db.SaveChanges();


              

            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al editar");
            }
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int Id)
        {
            try
            {
                
                    Models.Alumno oAlumno = _db.Alumno.Find(Id);
                    _db.Alumno.Remove(oAlumno);
                    _db.SaveChanges();
                
            }
            catch (Exception ex)
            {

                ModelState.AddModelError(ex.ToString(), "Error al Eliminar");
            }
            return Ok();
        }
    }
}