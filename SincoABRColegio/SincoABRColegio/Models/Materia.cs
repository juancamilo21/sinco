﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SincoABRColegio.Models
{
    public partial class Materia
    {
        public Materia()
        {
            Nota = new HashSet<Nota>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public int IdProfesor { get; set; }


        public virtual Profesor IdProfesorNavigation { get; set; }
        public virtual ICollection<Nota> Nota { get; set; }
    }
}
