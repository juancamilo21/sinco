﻿using System;
using System.Collections.Generic;

namespace SincoABRColegio.Models
{
    public partial class Alumno
    {
        public Alumno()
        {
            Nota = new HashSet<Nota>();
        }

        public int Id { get; set; }
        public string NombreCompleto { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
        public string Grado { get; set; }
        public DateTime FechaNacimiento { get; set; }

        public virtual ICollection<Nota> Nota { get; set; }
    }
}
