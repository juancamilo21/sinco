﻿using System;
using System.Collections.Generic;

namespace SincoABRColegio.Models
{
    public partial class Nota
    {
        public int Id { get; set; }
        public int IdAlumno { get; set; }
        public int IdMateria { get; set; }
        public int Periodo { get; set; }
        public int? Nota1 { get; set; }
        public int? Promedio { get; set; }

        public virtual Alumno IdAlumnoNavigation { get; set; }
        public virtual Materia IdMateriaNavigation { get; set; }
    }
}
