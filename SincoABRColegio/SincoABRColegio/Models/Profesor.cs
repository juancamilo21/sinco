﻿using System;
using System.Collections.Generic;

namespace SincoABRColegio.Models
{
    public partial class Profesor
    {
        public Profesor()
        {
            Materia = new HashSet<Materia>();
        }

        public int Id { get; set; }
        public string NombreCompleto { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }

        public virtual ICollection<Materia> Materia { get; set; }
    }
}
