﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ColegioSincoWeb.Helper
{
    public class WebApi
    {
        public HttpClient Inicial()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44377/");
            return client;
        }
    }
}
