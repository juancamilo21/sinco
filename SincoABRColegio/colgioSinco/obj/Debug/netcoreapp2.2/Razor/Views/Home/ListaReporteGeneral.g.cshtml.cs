#pragma checksum "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\Home\ListaReporteGeneral.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0c4d5df4fc766c3e7496fdfe7c7b301af981b91f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_ListaReporteGeneral), @"mvc.1.0.view", @"/Views/Home/ListaReporteGeneral.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/ListaReporteGeneral.cshtml", typeof(AspNetCore.Views_Home_ListaReporteGeneral))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\_ViewImports.cshtml"
using colgioSinco;

#line default
#line hidden
#line 2 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\_ViewImports.cshtml"
using colgioSinco.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0c4d5df4fc766c3e7496fdfe7c7b301af981b91f", @"/Views/Home/ListaReporteGeneral.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"84f369aa88b5273b9b8b6813bb66f36c9204be9f", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_ListaReporteGeneral : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<ColegioSincoWeb.Models.NotaViewModel>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-info"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("button"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Pdf", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("btnPdf"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(58, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\Home\ListaReporteGeneral.cshtml"
  
    ViewData["Title"] = "Reporte General";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
            BeginContext(158, 56, true);
            WriteLiteral("\r\n<h1 class=\"text-center\">Reporte General</h1>\r\n<br />\r\n");
            EndContext();
            BeginContext(214, 86, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0c4d5df4fc766c3e7496fdfe7c7b301af981b91f5048", async() => {
                BeginContext(281, 15, true);
                WriteLiteral("Generar Reporte");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(300, 432, true);
            WriteLiteral(@"
<br />
<br />
<table class=""table table-bordered"">
    <thead>
        <tr>
            <th>
                Alumno
            </th>
            <th>
                Materia
            </th>
            <th>
                Profesor
            </th>
            <th>
                Periodo
            </th>
            <th>
                Nota
            </th>
        </tr>
    </thead>
    <tbody>
");
            EndContext();
#line 34 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\Home\ListaReporteGeneral.cshtml"
 foreach (var item in Model) {

#line default
#line hidden
            BeginContext(764, 48, true);
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(813, 68, false);
#line 37 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\Home\ListaReporteGeneral.cshtml"
           Write(Html.DisplayFor(modelItem => item.IdAlumnoNavigation.NombreCompleto));

#line default
#line hidden
            EndContext();
            BeginContext(881, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(937, 61, false);
#line 40 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\Home\ListaReporteGeneral.cshtml"
           Write(Html.DisplayFor(modelItem => item.IdMateriaNavigation.Nombre));

#line default
#line hidden
            EndContext();
            BeginContext(998, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1054, 90, false);
#line 43 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\Home\ListaReporteGeneral.cshtml"
           Write(Html.DisplayFor(modelItem => item.IdMateriaNavigation.IdProfesorNavigation.NombreCompleto));

#line default
#line hidden
            EndContext();
            BeginContext(1144, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1200, 42, false);
#line 46 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\Home\ListaReporteGeneral.cshtml"
           Write(Html.DisplayFor(modelItem => item.Periodo));

#line default
#line hidden
            EndContext();
            BeginContext(1242, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1298, 40, false);
#line 49 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\Home\ListaReporteGeneral.cshtml"
           Write(Html.DisplayFor(modelItem => item.Nota1));

#line default
#line hidden
            EndContext();
            BeginContext(1338, 38, true);
            WriteLiteral("\r\n            </td>\r\n\r\n        </tr>\r\n");
            EndContext();
#line 53 "C:\Users\juanc\Source\Repos\SincoABRColegio\colgioSinco\Views\Home\ListaReporteGeneral.cshtml"
}

#line default
#line hidden
            BeginContext(1379, 24, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<ColegioSincoWeb.Models.NotaViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
