﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ColegioSincoWeb.Helper;
using ColegioSincoWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ColegioSincoWeb.Controllers
{
    public class NotaController : Controller
    {
        WebApi _api = new WebApi();


        public async Task<IActionResult> Index()
        {
            IEnumerable<NotaViewModel> Nota = null;
            HttpClient client = _api.Inicial();

            HttpResponseMessage res = await client.GetAsync("api/Nota");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Nota = JsonConvert.DeserializeObject<List<NotaViewModel>>(results);
            }
            
            return View(Nota);
        }

        public async Task<IActionResult> Details(int Id)
        {

            var Nota = new NotaViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/Nota/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Nota = JsonConvert.DeserializeObject<NotaViewModel>(results);
            }
            return View(Nota);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(NotaViewModel Nota)
        {
            HttpClient client = _api.Inicial();

            var postTask = client.PostAsJsonAsync<NotaViewModel>("api/Nota", Nota);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        public async Task<IActionResult> Delete(int Id)
        {
            var Code = new NotaViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.DeleteAsync($"api/Nota/{Id}");

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var Nota = new NotaViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/Nota/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Nota = JsonConvert.DeserializeObject<NotaViewModel>(results);
            }
            return View(Nota);

        }

        [HttpPost]
        public ActionResult Edit(NotaViewModel Nota)
        {
            HttpClient client = _api.Inicial();
            var putTask = client.PutAsJsonAsync<NotaViewModel>($"api/Nota", Nota);
            putTask.Wait();

            var result = putTask.Result;
            if (result.IsSuccessStatusCode)
                return RedirectToAction("Index");
            return View(Nota);


        }
        
    }
}