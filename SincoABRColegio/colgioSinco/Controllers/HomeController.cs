﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using colgioSinco.Models;
using ColegioSincoWeb.Models;
using System.Net.Http;
using Newtonsoft.Json;
using ColegioSincoWeb.Helper;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.pdf;

namespace colgioSinco.Controllers
{
    public class HomeController : Controller
    {
        WebApi _api = new WebApi();


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> ListaReporteGeneral()
        {
            IEnumerable<NotaViewModel> Nota = null;
            HttpClient client = _api.Inicial();

            HttpResponseMessage res = await client.GetAsync("api/Nota");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Nota = JsonConvert.DeserializeObject<List<NotaViewModel>>(results);
            }

            return View(Nota);
        }
        public async Task<IActionResult> Pdf()
        {


            IEnumerable<NotaViewModel> Nota = null;
            HttpClient client = _api.Inicial();

            HttpResponseMessage res = await client.GetAsync("api/Nota");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Nota = JsonConvert.DeserializeObject<List<NotaViewModel>>(results);
            }

            Document doc = new Document(PageSize.Letter,30f,20f,50f,40f);
            MemoryStream ms = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(doc, ms);
            writer.PageEvent = new HeaderFooter();
            doc.AddAuthor("");
            doc.AddTitle("ColegioSincoABR");
            doc.Open();

            PdfPTable table = new PdfPTable(5);
            table.WidthPercentage = 100f;
            table.HorizontalAlignment = Element.ALIGN_CENTER;

            PdfPCell _cell = new PdfPCell(new Paragraph("Alumno"));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell.BackgroundColor = BaseColor.LightGray;
            table.AddCell(_cell);

            _cell = new PdfPCell(new Paragraph("Materia"));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell.BackgroundColor = BaseColor.LightGray;
            table.AddCell(_cell);

            _cell = new PdfPCell(new Paragraph("Profesor"));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell.BackgroundColor = BaseColor.LightGray;
            table.AddCell(_cell);

             _cell = new PdfPCell(new Paragraph("Periodo"));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell.BackgroundColor = BaseColor.LightGray;
            table.AddCell(_cell);

            _cell = new PdfPCell(new Paragraph("Nota"));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            _cell.BackgroundColor = BaseColor.LightGray;
            table.AddCell(_cell);
            table.CompleteRow();


            foreach (var item in Nota)
            {

                PdfPCell alumno = new PdfPCell(new Paragraph(item.IdAlumnoNavigation.NombreCompleto));
                alumno.HorizontalAlignment = Element.ALIGN_CENTER;
                alumno.VerticalAlignment = Element.ALIGN_MIDDLE;
                table.AddCell(alumno);


                PdfPCell materia = new PdfPCell(new Paragraph(item.IdMateriaNavigation.Nombre));
                materia.HorizontalAlignment = Element.ALIGN_CENTER;
                materia.VerticalAlignment = Element.ALIGN_MIDDLE;
                table.AddCell(materia);


                PdfPCell profesor = new PdfPCell(new Paragraph(item.IdMateriaNavigation.IdProfesorNavigation.NombreCompleto));
                profesor.HorizontalAlignment = Element.ALIGN_CENTER;
                profesor.VerticalAlignment = Element.ALIGN_MIDDLE;
                table.AddCell(profesor);


                PdfPCell periodo = new PdfPCell(new Paragraph(item.Periodo.ToString()));
                periodo.HorizontalAlignment = Element.ALIGN_CENTER;
                periodo.VerticalAlignment = Element.ALIGN_MIDDLE;
                table.AddCell(periodo);

                PdfPCell nota = new PdfPCell(new Paragraph(item.Nota1.ToString()));
                nota.HorizontalAlignment = Element.ALIGN_CENTER;
                nota.VerticalAlignment = Element.ALIGN_MIDDLE;
                table.AddCell(nota);


            }
            doc.Add(table);

            writer.Close();
            doc.Close();

            ms.Seek(0, SeekOrigin.Begin);
            return File(ms, "application/pdf");

    
        }

    }
    class HeaderFooter : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            PdfPTable tbHeader = new PdfPTable(3);
            tbHeader.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
            tbHeader.DefaultCell.Border = 0;

            tbHeader.AddCell(new Paragraph());
            PdfPCell _cell = new PdfPCell(new Paragraph("Reporte General"));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.Border = 0;

            tbHeader.AddCell(_cell);
            tbHeader.AddCell(new Paragraph());

            tbHeader.WriteSelectedRows(0,-1,document.LeftMargin,writer.PageSize.GetTop(document.TopMargin)+40,writer.DirectContent);

            



            PdfPTable tbFooter = new PdfPTable(3);
            tbFooter.TotalWidth  = document.PageSize.Width - document.LeftMargin - document.RightMargin;
            tbFooter.DefaultCell.Border = 0;
            tbFooter.AddCell(new Paragraph());

            _cell = new PdfPCell(new Paragraph("Reporte General"));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.Border = 0;

            tbFooter.AddCell(_cell);

            _cell = new PdfPCell(new Paragraph("Pagina" + writer.PageNumber));
            _cell.HorizontalAlignment = Element.ALIGN_CENTER;
            _cell.Border = 0;

            tbFooter.AddCell(new Paragraph());

            tbFooter.WriteSelectedRows(0, -1, document.LeftMargin, writer.PageSize.GetBottom(document.BottomMargin) + 5, writer.DirectContent);


        }
    }
}

