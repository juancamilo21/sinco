﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ColegioSincoWeb.Helper;
using ColegioSincoWeb.Models;
using colgioSinco.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ColegioSincoWeb.Controllers
{
    public class MateriaController : Controller
    {
        WebApi _api = new WebApi();


        public async Task<IActionResult> Index()
        {

            IEnumerable<MateriaViewModel> Materia = null;
            HttpClient client = _api.Inicial();

            HttpResponseMessage res = await client.GetAsync("api/Materia");
            if (res.IsSuccessStatusCode)
            {
                
                var results = res.Content.ReadAsStringAsync().Result;

                Materia = JsonConvert.DeserializeObject<List<MateriaViewModel>>(results);
            }
            

            return View(Materia);
        }

        public async Task<IActionResult> Details(int Id)
        {

            var Materia = new MateriaViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/Materia/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Materia = JsonConvert.DeserializeObject<MateriaViewModel>(results);
            }
            return View(Materia);
        }

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public IActionResult Create(MateriaViewModel Materia)
        {
            HttpClient client = _api.Inicial();

            var postTask = client.PostAsJsonAsync<MateriaViewModel>("api/Materia", Materia);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        public async Task<IActionResult> Delete(int Id)
        {
            var Code = new MateriaViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.DeleteAsync($"api/Materia/{Id}");

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var Materia = new MateriaViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/Materia/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Materia = JsonConvert.DeserializeObject<MateriaViewModel>(results);
            }
            return View(Materia);

        }

        [HttpPost]
        public ActionResult Edit(MateriaViewModel Materia)
        {
            HttpClient client = _api.Inicial();
            var putTask = client.PutAsJsonAsync<MateriaViewModel>($"api/Materia", Materia);
            putTask.Wait();

            var result = putTask.Result;
            if (result.IsSuccessStatusCode)
                return RedirectToAction("Index");
            return View(Materia);


        }
    }
}