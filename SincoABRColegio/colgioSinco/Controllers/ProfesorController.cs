﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ColegioSincoWeb.Helper;
using ColegioSincoWeb.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ColegioSincoWeb.Controllers
{
    public class ProfesorController : Controller
    {
        WebApi _api = new WebApi();


        public async Task<IActionResult> Index()
        {
            IEnumerable<ProfesorViewModel> Profesor = null;
            HttpClient client = _api.Inicial();

            HttpResponseMessage res = await client.GetAsync("api/Profesor");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Profesor = JsonConvert.DeserializeObject<List<ProfesorViewModel>>(results);
            }
            return View(Profesor);
        }

        public async Task<IActionResult> Details(int Id)
        {

            var Profesor = new ProfesorViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/Profesor/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Profesor = JsonConvert.DeserializeObject<ProfesorViewModel>(results);
            }
            return View(Profesor);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(ProfesorViewModel Profesor)
        {
            HttpClient client = _api.Inicial();

            var postTask = client.PostAsJsonAsync<ProfesorViewModel>("api/Profesor", Profesor);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        public async Task<IActionResult> Delete(int Id)
        {
            var Code = new ProfesorViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.DeleteAsync($"api/Profesor/{Id}");

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var Profesor = new ProfesorViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/Profesor/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                Profesor = JsonConvert.DeserializeObject<ProfesorViewModel>(results);
            }
            return View(Profesor);

        }

        [HttpPost]
        public ActionResult Edit(ProfesorViewModel Profesor)
        {
            HttpClient client = _api.Inicial();
            var putTask = client.PutAsJsonAsync<ProfesorViewModel>($"api/Profesor", Profesor);
            putTask.Wait();

            var result = putTask.Result;
            if (result.IsSuccessStatusCode)
                return RedirectToAction("Index");
            return View(Profesor);


        }
    }
}