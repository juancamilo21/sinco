﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ColegioSincoWeb.Helper;
using colgioSinco.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ColegioSincoWeb.Controllers
{
    public class AlumnoController : Controller
    {
        WebApi _api = new WebApi();


        public async Task<IActionResult> Index()
        {
            IEnumerable<AlumnoViewModel> alumno = null;
            HttpClient client = _api.Inicial();
  
            HttpResponseMessage res = await client.GetAsync("api/Alumno");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                alumno = JsonConvert.DeserializeObject<List<AlumnoViewModel>>(results);
            }
            return View(alumno);
        }

        public async Task<IActionResult> Details(int Id)
        {

            var alumno = new AlumnoViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/Alumno/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                alumno = JsonConvert.DeserializeObject<AlumnoViewModel>(results);
            }
            return View(alumno);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(AlumnoViewModel alumno)
        {
            HttpClient client = _api.Inicial();

            var postTask = client.PostAsJsonAsync<AlumnoViewModel>("api/Alumno", alumno);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        public async Task<IActionResult> Delete(int Id)
        {
            var Code = new AlumnoViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.DeleteAsync($"api/Alumno/{Id}");

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var alumno = new AlumnoViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/Alumno/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                alumno = JsonConvert.DeserializeObject<AlumnoViewModel>(results);
            }
            return View(alumno);

        }

        [HttpPost]
        public ActionResult Edit(AlumnoViewModel alumno)
        {
            HttpClient client = _api.Inicial();
            var putTask  =  client.PutAsJsonAsync<AlumnoViewModel>($"api/Alumno", alumno);
            putTask.Wait();

            var result = putTask.Result;
            if (result.IsSuccessStatusCode)
                return RedirectToAction("Index");
            return View(alumno);
            

        }
    }
}