﻿using colgioSinco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioSincoWeb.Models
{
    public class NotaViewModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Nombre del Alumno")]
        public int IdAlumno { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Nombre de la Materia")]
        public int IdMateria { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Periodo")]
        public int Periodo { get; set; }

        [Display(Name = "Nota")]
        public int? Nota1 { get; set; }


        public int? Promedio { get; set; }


        public virtual AlumnoViewModel IdAlumnoNavigation { get; set; }
        public virtual MateriaViewModel IdMateriaNavigation { get; set; }
    }
}
