﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioSincoWeb.Models
{
    public class ProfesorViewModel
    {
        public ProfesorViewModel()
        {
            Materia = new HashSet<MateriaViewModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required(ErrorMessage ="El campo es Obligatorio")]
        [Display(Name ="Nombre Completo")]
        public string NombreCompleto { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name ="Fecha de ingreso")]
        [DataType(DataType.Date)]
        public DateTime FechaIngreso { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Telefono")]
        [MinLength(7, ErrorMessage = "debe tener minimo 7 digitos maximo 10")]
        [MaxLength(10, ErrorMessage = "debe tener minimo 7 digitos maximo 10")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage ="El email no es valido")]
        public string Email { get; set; }

        public virtual ICollection<MateriaViewModel> Materia { get; set; }
    }
}
