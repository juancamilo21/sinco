﻿using ColegioSincoWeb.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace colgioSinco.Models
{
    public class AlumnoViewModel
    {
        public AlumnoViewModel()
        {
            Nota = new HashSet<NotaViewModel>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Nombre Completo")]
        public string NombreCompleto { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Telefono")]
        [MinLength(7,ErrorMessage ="debe tener minimo 7 digitos maximo 10")]
        [MaxLength(10, ErrorMessage = "debe tener minimo 7 digitos maximo 10")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Direccion")]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "El email no es valido")]
        public string Email { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Grado")]
        public string Grado { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Nacimiento")]
        public DateTime FechaNacimiento { get; set; }

        public virtual ICollection<NotaViewModel> Nota { get; set; }
    }
}
