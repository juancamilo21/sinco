﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioSincoWeb.Models
{
    public class MateriaViewModel
    {
        public MateriaViewModel()
        {
            Nota = new HashSet<NotaViewModel>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int Id { get; set; }
        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }
        [Display(Name = "Profesor Asignado")]
        [ForeignKey("Profesor")]
        public int IdProfesor { get; set; }

        [ForeignKey("IdProfesor")]
        public virtual ProfesorViewModel IdProfesorNavigation { get; set; }
        public virtual ICollection<NotaViewModel> Nota { get; set; }
    }
}
