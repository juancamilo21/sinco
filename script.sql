USE [master]
GO
/****** Object:  Database [SincoABR]    Script Date: 21/06/2020 12:56:24 p.m. ******/
CREATE DATABASE [SincoABR]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SincoABR', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\SincoABR.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SincoABR_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\SincoABR_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [SincoABR] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SincoABR].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SincoABR] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SincoABR] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SincoABR] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SincoABR] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SincoABR] SET ARITHABORT OFF 
GO
ALTER DATABASE [SincoABR] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SincoABR] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SincoABR] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SincoABR] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SincoABR] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SincoABR] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SincoABR] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SincoABR] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SincoABR] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SincoABR] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SincoABR] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SincoABR] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SincoABR] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SincoABR] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SincoABR] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SincoABR] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SincoABR] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SincoABR] SET RECOVERY FULL 
GO
ALTER DATABASE [SincoABR] SET  MULTI_USER 
GO
ALTER DATABASE [SincoABR] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SincoABR] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SincoABR] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SincoABR] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SincoABR] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SincoABR', N'ON'
GO
ALTER DATABASE [SincoABR] SET QUERY_STORE = OFF
GO
USE [SincoABR]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [SincoABR]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 21/06/2020 12:56:25 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Alumno]    Script Date: 21/06/2020 12:56:26 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alumno](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NOT NULL,
	[Direccion] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[Grado] [varchar](50) NOT NULL,
	[FechaNacimiento] [date] NOT NULL,
 CONSTRAINT [PK_Alumno] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Auditoria]    Script Date: 21/06/2020 12:56:26 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Auditoria](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumeroInicial] [int] NULL,
	[NumeroFinal] [int] NULL,
	[FechaEjecucion] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Materia]    Script Date: 21/06/2020 12:56:26 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Materia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[IdProfesor] [int] NOT NULL,
 CONSTRAINT [PK_Materia] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Nota]    Script Date: 21/06/2020 12:56:28 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Nota](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdAlumno] [int] NOT NULL,
	[IdMateria] [int] NOT NULL,
	[Periodo] [int] NOT NULL,
	[Nota] [int] NULL,
	[Promedio] [int] NULL,
 CONSTRAINT [PK_Nota] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Profesor]    Script Date: 21/06/2020 12:56:29 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profesor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NombreCompleto] [varchar](50) NOT NULL,
	[FechaIngreso] [date] NOT NULL,
	[Telefono] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Profesor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Materia]  WITH CHECK ADD  CONSTRAINT [FK_Materia_Profesor] FOREIGN KEY([IdProfesor])
REFERENCES [dbo].[Profesor] ([Id])
GO
ALTER TABLE [dbo].[Materia] CHECK CONSTRAINT [FK_Materia_Profesor]
GO
ALTER TABLE [dbo].[Nota]  WITH CHECK ADD  CONSTRAINT [FK_Nota_Alumno] FOREIGN KEY([IdAlumno])
REFERENCES [dbo].[Alumno] ([Id])
GO
ALTER TABLE [dbo].[Nota] CHECK CONSTRAINT [FK_Nota_Alumno]
GO
ALTER TABLE [dbo].[Nota]  WITH CHECK ADD  CONSTRAINT [FK_Nota_Materia] FOREIGN KEY([IdMateria])
REFERENCES [dbo].[Materia] ([Id])
GO
ALTER TABLE [dbo].[Nota] CHECK CONSTRAINT [FK_Nota_Materia]
GO
/****** Object:  StoredProcedure [dbo].[sp_NumerosPrimosSinco]    Script Date: 21/06/2020 12:56:29 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Juan Camilo Peña Neita
-- Create date: 20/06/2020
-- Description:	creación de SP el cual recibe dos parametros y me devuelve al final los numeros primos dentro del rango introducido.
-- =============================================
CREATE PROCEDURE [dbo].[sp_NumerosPrimosSinco]
	--variables de entrada(rango de Consulta).
	@NumeroInicial int,
	@NumeroFinal int

AS
BEGIN
--se declaran dos variables mas para asiganarles los dos numeros iniciales que el usuario ingresa.
	DECLARE  @RangoInicial INT = @NumeroInicial,
         @RangoFinal INT = @NumeroFinal;

--Se declara una variable el cual se le asigna como valor el resultado de la resta, suma y division de los dos valores ingresados por el usuario
--usando el ceiling para redondear un numero hacia el proximo entero. 
DECLARE  @Cantidad INT = CEILING((@RangoFinal - @RangoInicial + 1) / 2.0)

;WITH primero AS
(
    SELECT  tmp.valor1
    FROM        (VALUES (0), (0), (0), (0), (0), (0), (0), (0), (0), (0)) tmp(valor1)
), segundo AS
(
    SELECT  0 AS [valor2]
    FROM        primero t1
    CROSS JOIN primero t2
    CROSS JOIN primero t3
), base AS
(
    SELECT  TOP( CONVERT( INT, CEILING(SQRT(@RangoFinal)) ) )
            ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS [num]
    FROM        segundo s1
    CROSS JOIN  segundo s2
), nums AS
(
    SELECT  TOP (@Cantidad)
            (ROW_NUMBER() OVER (ORDER BY (SELECT 1)) * 2) + 
                (@RangoInicial - 1 - (@RangoInicial%2)) AS [num]
    FROM        base b1
    CROSS JOIN  base b2
), divs AS
(
    SELECT  [num]
    FROM        base b3
    WHERE   b3.[num] > 4
    AND     b3.[num] % 2 <> 0
    AND     b3.[num] % 3 <> 0
)
SELECT  given.[num] AS [NumeroPrimo]
FROM        (VALUES (2), (3)) given(num)
WHERE   given.[num] >= @RangoInicial
UNION ALL
SELECT  n.[num] AS [NumeroPrimo]
FROM        nums n
WHERE   n.[num] BETWEEN 5 AND @RangoFinal
AND     n.[num] % 3 <> 0
AND     NOT EXISTS (SELECT *
                    FROM divs d
                    WHERE d.[num] <> n.[num]
                    AND n.[num] % d.[num] = 0
                    )
--Se valida si la tabla Auditoria existe en la base de datos, si ya existe se insertan los valores, si no existe se crea y se insertan los valores.
IF OBJECT_ID('Auditoria','U') IS NULL
BEGIN
	CREATE TABLE Auditoria 
	(
		Id INT IDENTITY(1,1) PRIMARY KEY,
		NumeroInicial INT,
		NumeroFinal INT,
		FechaEjecucion DATETIME
	)
	INSERT INTO Auditoria (NumeroInicial,NumeroFinal,FechaEjecucion)
	VALUES (@NumeroInicial,@NumeroFinal,SYSDATETIME())

END

ELSE 
BEGIN
	INSERT INTO Auditoria (NumeroInicial,NumeroFinal,FechaEjecucion)
	VALUES (@NumeroInicial,@NumeroFinal,SYSDATETIME())
END




END 





GO
USE [master]
GO
ALTER DATABASE [SincoABR] SET  READ_WRITE 
GO
